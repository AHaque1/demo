<?php

/**
 * Contoller class default method is do_index
 *  
 * class handles form submission via AJAX where its default method is do_index
 * 
 */

class validation {

	public function get_query($name = null){
		if(isset($name)){
			return (isset($_GET[$name]) && !empty($_GET[$name])) ? $_GET[$ame] : false ;
		} else {
			return $_GET;
		}

	}
	public function get_post($name = null){
		if(isset($name)){
			return (isset($_POST[$name]) && !empty($_POST[$name])) ? $_POST[$name] : false ;
			} else {
				return $_POST;
			}
	}

	public function is_ajax(){
			return
			(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true: false ;
	}

	public function do_index() {
			// process the ajax request only if found to be ajax
			if ($this->is_ajax()) {
				return $this->route_ajax();
			}
			//otherwise initiate Smarty template engine to output view with name of directory where templates are held and render once overloaded with properties
			$this->view = new View_Smarty();
			$this->view->setTemplateDir( __DIR__.'/views/' );
			$this->view->first_name_placeholder = "please enter first name";
			$this->view->last_name_placehodler = "please enter last name";
			//render the form
			return $this->view-render('main.tpl');

		}


	/**
	 * Process incoming ajax requests.
	 *
	 * @return string  Response to be returned to the requesting agent.
	 */
	protected function route_ajax() {

		$request_data = $this->get_post();

		if (isset($request_data['action'])) {
			switch ($request_data['action']) {
				case 'save':
					$this->save_return_object($incident->save_input($request_data));
					break;
				case 'submit':
					break;
				default:
					$this->save_return_object([false,'unknown action']);
			}
		} else {
			$this->save_return_object([false,'unknown action']);
		}
	}

	/**
	 * A method for returning the result of an ajax save to the browser
	 *
	 * @param array $action_results : [boolean,string]
	 */
	protected function save_return_object(array $action_results){
		echo json_encode([
			"success" => $action_results[0],
			"message" => $action_results[1]
		]);
		exit();
	}


    public function save_input($form_data)
    	{
    	//assign and remove/remove what is not needed anymore to reduce number of loops later
    		unset($form_data['action']);
    
    		$saving_result = 'An error saving.';
    		foreach ($form_data as $form_key => $form_value) {
    			switch ($form_key) {
    				case 'full_name':
    				    //validate method for name and empty string
    						if ($this->validate($form_key, $form_value))) {
    						    // save method saves form field data to database, if successfull ternary returns the array to feed the AJAX call with success info. 
    							return $this->save($form_key, $form_value) ? [ true , "true"] : [ false , "An error with saving. "];
    						} else {
    								return [false, "An error with saving."];
    						}
    				break;
    
    				case 'contact':
    				    //validate method for contact number and empty string
    					if ($this->validate($form_key, $form_value))) {
    						return $this->save($form_key, $form_value) ? [ true , "true"] : [ false , "An error with saving. "];
    					} else {
    							return [false, "An error with saving."];
    					}
    					break;
    			default:
    					return [ false , 'Unknown field'];
    		} //end of switch
    		} //end of foreach
    
    		return [ false , $saving_result];
    	}

	public function validate(){
		//do validation e.g check if int, date, email etc
		//validate methods provided by zend framework
		return true;
	}

	}

?>
