<!DOCTYPE html>
<html>
<head>
<title>demo</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.4/angular.min.js">
</script>
  <link rel="stylesheet" href="/demo/css/demo.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="/demo/css/validate.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="/demo/css/notices.css" type="text/css" media="screen" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
  <header>
    <div class="header_pic">
      <section>
        <div class="header_inside font">
        <h1 class="title first">Anwaral</h1>
        <h1 class="title second">demo</h1>
          <section>
            <p>
              Hello, this is a quick page I put together to showcase two javascript examples below.
            </p>
          </section>
      </section>
    </div>
      <!-- <picture>
    <source srcset="../site/images/benchwide1600.jpg" media="(max-width: 768px)">
    <source srcset="../site/images/benchwide1600.jpg" media="(max-width: 1600px)">
    <img srcset="http://www.capturestills.com/site/images/amailboxs.jpg" alt="My default image">
      </picture> -->

    </div>
  </header>
  <div class="content font">
    <h1 class="paragraph_title">Custom Validation</h1>
    <p>This form validation was inspired by the JQuery validate plugin. Our comprised 4 form pages preloaded and used JS to moving between forms. Validation would need to occur on the 'onchange' event of specified fields and on successfull validation and an AJAX submission commences. The Jquery plugin was unable to serve this with its options therefore instead of revising its source I decided to create our own</p>
    <div class="panel">
    <div id="form_container">
      <form>
        <div class="input_container">
          <input type="text" name="firstname" placeholder="please enter Name" class="form__field">
        </div>
        <div class="input_container">
          <input type="text" name="contact" placeholder="Please enter contact number" class="form__field">
        </div>
      </form>
      <button class="code_toggle">view code</button>
    </div>

    <div id="val_code">
      <a target="_blank" href="https://bitbucket.org/AHaque1/demo/src/da054664eeae400d53622e5f7d7a0e49074402e6/?at=development">Bitbucket</a>
    </div>
  </div>
  </div>
</body>
    <script type="text/javascript" src="/demo/js/validation.js"></script>
      <script type="text/javascript" src="/demo/js/notices.js"></script>
        <script type="text/javascript" src="/demo/js/demo.js"></script>
    <footer></footer>
</html>