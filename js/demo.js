! function() {
  var header_inside = $(".header_inside");
  var content_position = $(".content").offset().top;
  var header_marker = false;
  $(window).scroll(function() {
    var scrollTop = $(window).scrollTop();
    var translate_pos = (scrollTop * 2 < $(".header_pic").width() / 2) ? scrollTop * 2 : $(".header_pic").width() / 2;
    var translate_pos_height = content_position / scrollTop;
    $(".header_inside").css({
      'opacity': 1 - scrollTop / 500,
      'margin-top': scrollTop / 2 + 'px'
    });
    if (scrollTop > content_position / 6) {
      $(".title").css({
        'transform': "translateY(-" + scrollTop / 2 + "px)"
      });
      if (!header_marker) {
        $("#top_header .top_header_inner h1").html($(".title").eq(0).html()).css("transform", "translateY(0px)");
        header_marker = true;
      }
    } else {
      if (header_marker) {
        $("#top_header .top_header_inner h1").css("transform", "translateY(50px)");
          header_marker = false;
      }
        $(".title").css({
        'transform': "translateY(0px)"
      });
    }
  });
}();
