!function(){
  var instruction_pops = {
  element: false,
  message: false,
  element_position: false,
  callback: false,
  position: "bottom",
  element_array_reset: function(_array) {
    var new_arr = _array.filter(function() {
      return true;
    });
    return new_arr;
  },
  check_local_storage:function(){
    console.log(typeof window.localStorage.getItem("status"));
    console.log(window.localStorage.getItem("status"));
    return (typeof window.localStorage.getItem("status") !== null &&  window.localStorage.getItem("status") === "stop" ) ? true : false ;
  },
  init: function(options, callback) {
    /**
     * Any preperation and variable/option assignment should occur here
     */
    if (Object.prototype.toString.call(options) != '[object Array]' && options.length > 0) {
      return;
    }
    if (!options[0].hasOwnProperty("element") && options[0].element instanceof $ && options[0].element.length > 0 && !options[0].hasOwnProperty("message")) {
      console.log("element or message not available");
      return;
    }
    this.number = options[0].number;
    this.element = options[0].element.eq(0);
    this.message = options[0].message;
    this.element_position = this.element.offset().top;
    this.element_position_left = this.element.offset().left;
    (options[0].hasOwnProperty("position") && ["top", "bottom", "center"].indexOf(options[0].position) != -1) ? this.position = options[0].position : this.position = "bottom";

    if (typeof callback != 'undefined' && typeof callback == 'function') {
      this.callback = callback;
    }
    this.start_notices(options);
  },
  start_notices: function(options) {

    $("<div></div>", {
        "id": "guide_notice",
        class: "notice_top"
      })
      .html(this.message)
      .append($("<p></p>", {
        class: "notice_button",
        style: "cursor:pointer;"
      }).html("next"))
      .append($("<input>", {
        type: "checkbox",
        name: "stop_notice",
        id: "stop_notice"
      }))
      .appendTo("body")
      .on("click", function(e) {
        var notice_status = ($("#stop_notice").is(":checked")) ? "stop" : instruction_pops.number;
        if (e.target.className == 'notice_button') {
          $(this).off();
          if (typeof instruction_pops.callback != 'undefined' && typeof instruction_pops.callback == 'function') {
            instruction_pops.callback(notice_status);
          }
          options.splice(0, 1);
          $("#guide_notice").fadeOut("slow");
          setTimeout(function() {
            if ($("#guide_notice").length > 0) {
              $("#guide_notice").remove();
              if (options.length > 0 && notice_status !== "stop") {
                option = instruction_pops.element_array_reset(options)
                instruction_pops.init(options);
              } else {
                instruction_pops.callback("stop");
                $("body").removeClass("stop");
              };
            }
          }, 1000);
        }
      });

    var notice_class = "notice_" + this.position;
    if (this.position === 'center') {
      $("#guide_notice").removeClass("notice_top");
      $("#guide_notice").addClass(notice_class);
      $("." + notice_class).fadeIn("slow");
      $("." + notice_class).css({
        "top": "50%",
        "left": "calc( 50% - " + ($("#guide_notice").width() / 2) + "px)"
      });
    } else {
      var div_height = (this.element_position - ($("#guide_notice").eq(0).outerHeight() + 10));
      if (div_height < 0 || this.position == 'top') {
        div_height = this.element_position + (this.element.outerHeight() + 10);
        notice_class = 'notice_top';
      }
      $("#guide_notice").addClass(notice_class);
      $("." + notice_class).fadeIn("slow");
      $("." + notice_class).css({
        "top": div_height + "px",
        "left": this.element_position_left
      });
    }
    this.element
    $('body').animate({
      scrollTop: this.element.offset().top
    }, 500)
    $("body").addClass("stop");
  }
  }

//the callback for init callback parameter
function notices_callback(status) {
  console.log(status);
    (typeof status == 'number' || ["stop"].indexOf(status) != ' undefined' ) ?  window.localStorage.setItem("status", status) : console.log('error with status');
}
console.log(instruction_pops.check_local_storage());
if(instruction_pops.check_local_storage()){
  console.log(instruction_pops.check_local_storage());
  return;
}
//initiate notices
instruction_pops.init(
  [{
    "number": 1,
    "element": $("body"),
    "message": "Hello and thank you for viewing this demo. I have these guide notices as one of the two javascript examples to demo on here. On viewing or clicking cancel values are set in localStorage for this demo which otherwise would be stored in a database",
    "position": "center"

  },
 {
    "number": 2,
    "element": $(".input_container"),
    "message": "Alpha characters only validation triggered on change. Code is available to view on Bitbucket(link below)"
  }],
  notices_callback

);
}();
