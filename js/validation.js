
!function(){
var bc = bc || {};
		bc.val = {
		errors: [], //errors to output to display
		clean_errors: [], //errors that are no longer invalid held here for removal from display
		tagtypes: ['input', 'select', 'textarea'], //on .form__field class with listed tagtypes are permitted, those not present in this array will be ignored
		ignore_list: { // tags that should be ignored. e.g. text area will NOT be submitted on changed, it is instead polled therefore name attribute is added to ignore list other it will validate and submit on change too
		},
		settings: { //default settings here can be added to by including additional settings when calling start method. (functionality yet to be added)
			post_name: "name", //will be identify elements on there name attribute
			rules: { //rules for each specified name of element desired for form submission or validation
      	firstname:{
	      	required:true,
	      	validate:"names"
    		},
				contact:{
					required:true,
					validate: "phone"
				}
			},
			//default error displaying method. Displays under the input element
			default_display_errors_method: function(element, message ) {
				var class_name = "errors";
				//if error message already exists then we just need to change its text
				if ($(element).parent().find('.'+class_name).length > 0) {
					$(element).parent().find('.'+class_name+' p').html(message);
					$(element).focus();
				} else {
					$(element).parent().append(
						$('<div>', {
							class: class_name
						}).append(
							$('<p>').html(message)
						)
					);

					var perc = 100 - Math.ceil((($(element).parent().find(".errors p").width() / $(element).parent().find(".errors").width()) * 100)) - 8;
					var p_width = $(element).parent().find(".errors p").outerWidth();
					var err_width = $(element).parent().find(".errors").outerWidth();
					var translate_amount = ((100 - ((p_width /err_width) * 100)) - 12 < 1 ) ? 0 : (100 - ((p_width /err_width) * 100)) - 12 ;
					$(element).focus().parent().css("overflow", "hidden").find('.errors').eq(0).css("transform", "translateX(" + translate_amount + "%)");
				}
			}
		},
		messages: {
			phone: "please enter a valid contact number", //KEYS refer to validate: KEYS in settings.rules with names that have validate key
			names: "Letters only please "
		},
		//used to clear errors array of an error that already exists so it is not included twice
		check_if_element_exists_in_errors_array: function(element, remove) {
			if (Object.prototype.toString.call(this.errors) == '[object Array]') {
				var index = false;
				$.each(this.errors, function(k, v) {
					if (v.element == element) {
						index = k;
					};
				});
				if (typeof remove != 'undefined' && remove) {
					return (index !== false) ? this.errors.splice(index, 1) : false;
				} else {
					return index;
				}

			};
		},
		methods: {
			required: function(element) {
				var value = element.value;
				if (element.nodeName.toLowerCase() === "select") {
					// Could be an array for select-multiple or a string, both are fine this way
					var val = $(element).val();
					return val && val.length > 0;
				}
				return value.length > 0;
			},
			names: function(element){
			    var value = element.value;
			    console.log(value);

				return /^[a-zA-Z]{1,15}\s{0,1}([a-zA-Z\s]{1,15}){0,5}$/.test(value);
			},
			phone:function(element){

				var value = element.value;
				console.log(value);
				console.log(/^\+?[0-9]{3}-?[0-9]{6,12}$$/.test(value));
			return /^\+?[0-9]{3}-?[0-9]{6,12}$/.test(value);
			}

		},

		check: function(element) {
			var rules = {}; //start with a blank rules object. Rules are set for each checked element and are either required indicated which will call the required method and validator will call the custom method if it is set
			if (typeof this.settings.rules == 'object') {
				//set an internal marker to the type of validation check. if email the vals.email(vals[check_type]) will be called
				if (this.settings.rules.hasOwnProperty(element[this.settings.post_name])) { // post_name is planned to configurable and is currently set to the name attribute, so marked fields' name attr will be pulled throughout this process
					if (this.settings.rules[element[this.settings.post_name]].hasOwnProperty("required")) {
						if(this.settings.rules[element[this.settings.post_name]].required === true){
							rules['required'] = this.methods.required;
						}
					};
					if (this.settings.rules[element[this.settings.post_name]].hasOwnProperty("validate")) {
						var validate_method_in_settings = this.settings.rules[element[this.settings.post_name]].validate;
						if (this.methods.hasOwnProperty(validate_method_in_settings)) {
							rules["validator"] = this.methods[validate_method_in_settings];
						}
					}
				}
			}
			return (Object.keys(rules).length > 0) ? rules : false; //this object is passed to the below function validate
		},

		validate: function(element, rules) {
			if (typeof rules != 'object' || Object.keys(rules).length < 1) {
				return false;
			}
			if (rules.hasOwnProperty('required') && !rules.required(element)) { // the rules object argument is the return of check method above
				this.check_if_element_exists_in_errors_array.call(this, element, true); // if current element already exists remove it from errors array
				this.errors.push({
					element: element,
					message: 'Value Required'
				});

			} else if (rules.hasOwnProperty('validator') && !rules.validator.call(this, element)) {
				console.log(element[this.settings.post_name]);
				console.log(this.settings.rules[element[this.settings.post_name]]);
				this.check_if_element_exists_in_errors_array.call(this, element, true);
				this.errors.push({
					element: element,
					message: this.messages.hasOwnProperty(this.settings.rules[element[this.settings.post_name]].validate) ? this.messages[this.settings.rules[element[this.settings.post_name]].validate] : 'invalid value'
				});
			} else { //if element passes validation remove it from errors array if it is in error array and add it to clean_errors array to clear it from display
				var len = this.errors.length;
				for (var i = len - 1; i >= 0; i--) {
					if (this.errors[i].element == element) {
						this.clean_errors.push(element);
						this.errors.splice(i, 1);
						break; //validate is done per element so break on first match
					};
				}
			};
		},
		/**
		 * Form element-type setter methods.
		 *
		 * Each element-type pair has its own unique way of retrieving the values they hold
		 * so they can be sent to the server.
		 */
		tags: {
			input: { //each element is checked against the tagtypes array if a match is found then it must have type to match to .e.g. input type="text" which calls the corresponding input.text method
				'text': function(element) {
					var val = element.value, check = this.check(element);
					if (typeof check == 'object') { //check will return an object or it will return false
						this.validate.call(bc.val, element, check);
						this.data[element[this.settings.post_name]] = val;
						if (this.ajax_call && Object.keys(this.data).length > 0) {
							this.send_ajax(element, this.data, 'save');
						}
					} else {
						console.log($(element).attr("name") + "element has not been included in settings");

					};
				},
				'radio': function(element) { },
				'checkbox': function(element) { }
			},
			textarea: {
				"textarea": function(element) { }
			},
			select: {
				"select-one": function(element) { },
				"select-multiple": function(element) { }
			}
		},


		/**
		 * Checks if we have methods defined to handle the form element then call those methods.
		 *
		 * Checking is made against tagtypes property firsts and if passed
		 * tags property is being checked for a specific type. For example 'input' tag
		 * can have types like 'text', 'radio' etc.
		 *
		 * If these checks a function under tags.type() is being invoked with the form
		 * element as a parameter so it can be read and trigger an ajax call.
		 *
		 * @param form_element
		 * @param nsettings Currently unused... TODO check if necessary
		 * @returns {boolean}
		 */
		go: function(form_element, nsettings) {

			//check if element name is allowed
			if (this.tagtypes.indexOf(form_element.tagName.toLowerCase()) != -1) {

				var tag = this.tagtypes[this.tagtypes.indexOf(form_element.tagName.toLowerCase())];
				if (this.tags[tag].hasOwnProperty(form_element.type)) { //e.g. tags.input.text
					//check if we have a way to handle the specific type of the element, if yes invoke it
					this.tags[tag][form_element.type].call(this, form_element); //call tags.input.text()
				}
			}
		},

		/**
		 * Perform send action.
		 * @param element  Element we send the data from ?
		 * @param _data    Data to be passed to the controller.
		 * @param action   Action to be performed by the controller.
		 */
		send_ajax: function(element, _data, action) { //_data could be this.data which is data from onchange event or this.stored_data which will be the checkboxes data stored for submission on next click
				console.log("AJAX send");
		},

		/**
		 * Runs the validation and form submission via ajax(ajax_call = true) process
		 *
		 * This is being invoked each time an onchange event is being triggered
		 * on any of the form elements on the current page.
		 *
		 * @param field_elems  All form elements that have watchers on them.
		 * @param e            Event attached to specific form element.
		 */
		event_callback_change: function(field_elems, e) {
			this.ajax_call = true;
			if (field_elems.indexOf(e.target) != -1) {

				//if event target is found in the element list
				this.data = {};
				var fields = $(e.target);
				fields.each(function(prop_name, form_element) {
					this.go(form_element); //follow this to see how element is being handled
				}.bind(this));
			}
			this.output_errors();
			this.error_clean();
		},

		output_errors: function(ajax_error) { //checks rules for element to see if it has own method for displaying errors otherwise it display error via defualt method in settings.error_out
			var error_type = 'errors';
			// this[error_type] refers to errors property array
			$.each(this[error_type], function(index, value) {
				if (this.settings.rules.hasOwnProperty(value.element.name) && this.settings.rules[value.element.name].hasOwnProperty('error_location')) {
					this.settings.rules[value.element.name].error_location(value.element, value.message);
				} else {
					this.settings.default_display_errors_method(value.element, value.message);
				}
			}.bind(this));
		},

		error_clean: function() {
			$.each(this.clean_errors, function(index, value) {
				if (this.settings.rules.hasOwnProperty(value.name) && this.settings.rules[value.name].hasOwnProperty('error_clear')) {
					this.settings.rules[value.name].error_clear(value);
				} else {
					if ($(value).parent().find('.errors').length > 0) {
						$(value).parent().find('.errors').css("transform", "translateX(100%)").fadeOut();
						setTimeout(function() {
							$(value).parent().find('.errors').remove();
						}, 1000);
					}
				}
			}.bind(this));
			bc.val.clean_errors = [];
		},

		/**
		 * Main function, initiates validation and send outs.
		 * Attaches events to form elements in the end.
		 *
		 * @param options
		 * @var current_panel the panel where the current form to be validated lies within

		 * @returns {boolean} false if options param does not contian current_pan property, null otherwise
		 */
		main: function(options) {
			//get incident statement id from the current panel only
			var current_panel = options.current_pan;
			var fields = options.current_pan.find('.form__field');
			var field_elems = [];

			//validate all the form fields with class .form_field to check if they contain the neccessary attributes
			fields.each(function(k, v) {
				//check if element is in ignore list if so do not push it to array
				if (v.hasAttribute('name') && bc.val.ignore_list.hasOwnProperty(v.name)) {
					//if ignored element has a callback, call it
					if (bc.val.ignore_list[v.name].hasOwnProperty('callback')) {
						bc.val.ignore_list[v.name].callback.call(bc, v);
					}
				} else {
					field_elems.push(v); //after passing validation push to array
				}
			});
				options.current_pan.on("change", this.event_callback_change.bind(this, field_elems));

		}
	};


	/**
	 * Start bc val.
	 */
	bc.val.main({
		//use the panel that is not hidden
		current_pan: $(".panel")

	});

$(".code_toggle").click(function(){
$("#val_code").toggle("slow");

});



}();
